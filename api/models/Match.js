const mongoose = require('mongoose');

const MatchSchema = mongoose.Schema({
    homeId : {
        type: String,
        required: true
    },
    homeName : {
        type: String,
        required: true
    },
    awayId : {
        type:  String,
        required: true
    },
    awayName : {
        type:  String,
        required: true
    },
    competitionType: {
      type:  String,
      required: true
    },
    competitionState: {
        type: String,
        required: true
    },
    round: {
        type: Number,
        required: false
    },
    group: {
      type: String,
      default: 'none'
    },
    homeGoals : {
        type: Number,
        default: 0
    },
    matchState: {
        type: String,
        default: "NEW"
    },
    awayGoals : {
        type: Number,
        default: 0
    },
    firstThirdScore: {
        type: String,
        default: '0:0'
    },
    secondThirdScore: {
        type: String,
        default: '0:0'
    },
    thirdThirdScore: {
        type: String,
        default: '0:0'
    }
});

module.exports = mongoose.model('match', MatchSchema);
