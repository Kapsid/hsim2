const mongoose = require('mongoose');

const TeamSchema = mongoose.Schema({
  name : {
    type: String,
    required: true
  },
  countryCode:{
    type: String,
    required: true
  },
  group : {
    type: String,
    required: false
  },
  rating : {
    type: Number,
    required: true
  },
  power : {
    type: Number,
    required: true
  },
  competitionType: {
    type: String,
    required: true
  },
  goalsFor : {
    type: Number,
    default: 0
  },
  goalsAgainst : {
    type: Number,
    default: 0
  },
  points : {
    type: Number,
    default: 0
  },
  played: {
    type: Number,
    default: 0
  },
  wins : {
    type: Number,
    default: 0
  },
  draws : {
    type: Number,
    default: 0
  },
  lossesInOT : {
    type: Number,
    default: 0
  },
  losses : {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model('teams', TeamSchema);