const mongoose = require('mongoose');

const CompetitionSchema = mongoose.Schema({
  name : {
    type: String,
    required: true
  },
  year : {
    type: Number,
    required: true
  },
  type : {
    type: String,
    required: true
  },
  status : {
    type: String,
    required: true
  },
  country : {
    type: String,
    required: true
  },
  countryCode : {
    type: String,
    required: true
  },
  places: {
    type: Array,
    required: true
  }
});

module.exports = mongoose.model('competition', CompetitionSchema);