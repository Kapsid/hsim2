const Team = require('../models/Teams');
const Match = require('../models/Match');

/**
 * Getting groups for teams
 * @param competitionType
 * @param group
 * @returns {Promise<*>}
 */
async function getGroupTeams(competitionType,group){
    const teams = await Team
        .find({
            'competitionType': competitionType,
            'group': group
        });

    return teams;
}

/**
 * Credit by: https://codereview.stackexchange.com/questions/159817/javascript-match-scheduling-fixture-generation-algorithm
 * @param n
 * @param j
 * @returns {number[]}
 */
function round(n, j) {
    let m = n - 1;
    let round = Array.from({length: n}, (_, i) => (m + j - i) % m); // circular shift
    round[round[m] = j * (n >> 1) % m] = m; // swapping self-match
    return round;
}

/**
 * https://codereview.stackexchange.com/questions/159817/javascript-match-scheduling-fixture-generation-algorithm
 * @param n
 * @param teams
 * @returns {{id: *, matches: *[]}[]}
 */
function generateMatchesForTeam(n,teams) {
    let rounds = Array.from({length: n - 1}, (_, j) => round(n, j));
    return Array.from({length: n}, (_, i) => ({
        id: teams[i].id,
        name: teams[i].name,
        matches: rounds.map(round => teams[round[i]])
    }));
}

/**
 * Drawing group matches
 * @param teams
 * @returns {Promise<void>}
 */
function drawGroupMatches(teams) {

    return generateMatchesForTeam((teams.length),teams);
}

/**
 * Generating matches
 * @param teamsWithMatches
 * @param rounds
 * @param group
 * @param competitionType
 * @returns {[]}
 */
async function generateGroupMatches(teamsWithMatches, rounds, group, competitionType) {
    for(let round = 0; round < rounds - 1; round++){
        let teamsAlreadyPlayed = [];
        for (let teamsCount = 0; teamsCount < teamsWithMatches.length; teamsCount++){
            if(!teamsAlreadyPlayed.includes(teamsWithMatches[teamsCount].name) && !teamsAlreadyPlayed.includes(teamsWithMatches[teamsCount].matches[round].name)){

                let homeTeam = teamsWithMatches[teamsCount];
                let awayTeam = teamsWithMatches[teamsCount].matches[round];

                // Switch for teams to still not play same home/away
                /*if(round % 2 !== 0){
                    homeTeam = awayTeam;
                    awayTeam = homeTeam;
                }*/

                const match = new Match({
                    'homeId': homeTeam.id,
                    'homeName': homeTeam.name,
                    'awayId': awayTeam.id,
                    'awayName': awayTeam.name,
                    'group': group,
                    'competitionState': 'GROUP',
                    'competitionType': competitionType,
                    'round': round
                });
                await match.save();

                teamsAlreadyPlayed.push(teamsWithMatches[teamsCount].name);
                teamsAlreadyPlayed.push(teamsWithMatches[teamsCount].matches[round].name);
            }
        }
    }
    return true;
}

/**
 * Generating one match
 * @param phase
 * @param competitionType
 * @returns {Promise<void>}
 */
async function generateWCPlayOffMatch(phase, competitionType) {

    const match = new Match({
        'homeId': 'unknown',
        'homeName': 'unknown',
        'awayId': 'unknown',
        'awayName': 'unknown',
        'group': 'none',
        'competitionState': phase,
        'competitionType': competitionType
    });
    await match.save();
    return true;

}

/**
 * Generating all PO phases matches
 * @returns {[]}
 */
async function generateWCPlayOffMatches(competitionType) {

    // Generating quarters
    for(let i = 0; i < 4; i++){
       await generateWCPlayOffMatch('QUARTERFINALS', competitionType);

       if(i < 2){
           await generateWCPlayOffMatch('SEMIFINALS', competitionType);
       }
    }

    // Generating 3rd place
    await generateWCPlayOffMatch('THIRDPLACE', competitionType);

    // Generating finals
    await generateWCPlayOffMatch('FINALS', competitionType);

    return true;
};

/**
 * Getting teams for groups, drawing groups, than drawing matches, than drawing PO matches
 * @param competitionType
 * @returns {Promise<{A: *, B: *}>}
 */
exports.drawMatches = async function (competitionType) {
    const teamsA = await getGroupTeams(competitionType,'A');
    const teamsB = await getGroupTeams(competitionType,'B');

    const matchesA = drawGroupMatches(teamsA);
    const matchesB = drawGroupMatches(teamsB);

    await generateGroupMatches(matchesA,teamsA.length, 'A', competitionType);
    await generateGroupMatches(matchesB,teamsB.length,'B', competitionType);

    //await generateWCPlayOffMatches(competitionType);

    //TODO UPDATE COMPETITION TYPE TO GROUP

    return { 'result' : 'Matches drawn' };
};

exports.divideMatches = async function (teams) {
    for(let index = 0; index < teams.length; index++){
        let group = 'A';

        if(index % 2 !== 0){
            group = 'B';
        }

        await Team.findByIdAndUpdate(teams[index].id, {$set: {'group':group}}, {new: true});
    }

    return true;
};

