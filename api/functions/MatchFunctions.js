const Team = require('../models/Teams');
const Match = require('../models/Match');

/**
 * Function for calculating goals
 * @param homeInfo
 * @param awayInfo
 * @param match
 * @returns {{homeGoals: *, awayGoals: *}}
 */
function calculateGoals(homeInfo, awayInfo, match) {

    let homeGoals = Math.floor(Math.random() * 8);
    let awayGoals = Math.floor(Math.random() * 8);

    let difference = homeInfo.power - awayInfo.power;
    let strongerGoals = homeGoals;
    let weakerGoals = awayGoals;
    let stronger = 'HOME';

    if(difference < 0) {
        difference = awayInfo.power - homeInfo.power;
        strongerGoals = awayGoals;
        weakerGoals = homeGoals;
        stronger = 'AWAY';
    }

    // Making power manipulations
    while(difference > 0 && strongerGoals <= weakerGoals){
        difference = difference - 3;
        homeGoals = Math.floor(Math.random() * 8);
        awayGoals = Math.floor(Math.random() * 8);

        if(stronger === 'HOME'){
            strongerGoals = homeGoals;
            weakerGoals = awayGoals;
        }

        else{
            strongerGoals = awayGoals;
            weakerGoals = homeGoals;
        }
    }

    return setNewTeamsData(homeGoals,awayGoals,homeInfo,awayInfo, match);
}

/**
 * Updating specific team
 * @param teamGoals
 * @param rivalGoals
 * @param teamInfo
 * @returns {{}}
 */
function setUpdatedTeamData(teamGoals,rivalGoals,teamInfo) {
    const teamNewInfo = {};

    teamNewInfo.goalsFor = teamGoals + teamInfo.goalsFor;
    teamNewInfo.goalsAgainst = rivalGoals + teamInfo.goalsAgainst;
    teamNewInfo.wins = teamInfo.wins;
    teamNewInfo.draws = teamInfo.draws;
    teamNewInfo.losses = teamInfo.losses;
    teamNewInfo.points = teamInfo.points;
    teamNewInfo.played = 1 + teamInfo.played;

    return teamNewInfo;
}

/**
 * Function for setting new data
 * @param homeGoals
 * @param awayGoals
 * @param homeInfo
 * @param awayInfo
 * @param match
 * @returns {{homeGoals: *, awayGoals: *}}
 */
async function setNewTeamsData(homeGoals,awayGoals,homeInfo,awayInfo, match) {

    // Home is winner
    if(homeGoals > awayGoals){
        homeInfo.wins += 1;
        homeInfo.points += 2;
        awayInfo.losses += 1;
    }

    // Draw
    else if(homeGoals === awayGoals){
        homeInfo.draws += 1;
        homeInfo.points += 1;
        awayInfo.draws += 1;
        awayInfo.points += 1;
    }

    // Away is winner
    else{
        awayInfo.wins += 1;
        awayInfo.points += 2;
        homeInfo.losses += 1;
    }

    // Updating match
    const matchFields = {};
    matchFields.homeGoals = homeGoals;
    matchFields.awayGoals = awayGoals;
    matchFields.matchState = 'PLAYED';

    // Updating home team
    const homeTeamFields = setUpdatedTeamData(homeGoals,awayGoals,homeInfo);

    // Updating away team
    const awayTeamFields = setUpdatedTeamData(awayGoals,homeGoals,awayInfo);

    await Match.findByIdAndUpdate(match.id, {$set: matchFields}, {new: true});
    await Team.findByIdAndUpdate(match.homeId, {$set: homeTeamFields}, {new: true});
    await Team.findByIdAndUpdate(match.awayId, {$set: awayTeamFields}, {new: true});

    return {
        homeGoals,
        awayGoals
    }
}

exports.getCalculatedMatch = function (matchObject, homeTeam, awayTeam) {

    return { homeGoals, awayGoals} = calculateGoals(homeTeam, awayTeam, matchObject)
};
