const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../models/User');

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: Users management
 */

/**
 * @swagger
 *
 * /users:
 *   post:
 *     description: Register of a user
 *     tags: [Users]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: Username
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: User's e-mail.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: got token
 *       400:
 *         description: invalid credentials
 *       500:
 *         description: server error
 */
router.post('/', [
        check('name','Name is required').not().isEmpty(),
        check('email','Please include a valid mail').isEmail(),
        check('password','Please enter a pass with 6 or more chars').isLength({min:6})
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array()});
        }

        const { name, email, password } = req.body;

        try{
            let user = await User.findOne({email});

            if(user){
                return res.status(400).json({ msg: 'User already exists'});
            }

            user = new User ({
                name,
                email,
                password
            });

            const salt = await bcrypt.genSalt(10);

            user.password = await bcrypt.hash(password, salt);

            await user.save();

            const payload = {
                user: {
                    id: user.id
                }
            };

            jwt.sign(payload, config.get('jwtSecret'), {
                expiresIn: 360000
            }, (err, token) => {
                if(err) throw err;
                res.json({token});
            });
        }
        catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    });

module.exports = router;