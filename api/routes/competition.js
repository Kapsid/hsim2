
const express = require('express');
const router = express.Router();

const Competition = require('../models/Competition');

/**
 * @swagger
 * tags:
 *   name: Competitions
 *   description: Competitions management
 */

/**
 * @swagger
 *
 * /competitions:
 *   get:
 *     description: Getting all competitions
 *     tags: [Competitions]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returned list of competitions
 *       500:
 *         description: server error
 */
router.get('/', async(req, res) => {
  try{
    const competitions = await Competition.find();
    res.json(competitions);
  } catch (err) {
    console.error((err.message));
    res.status(500).send('Server Error');
  }
});

/**
 * @swagger
 *
 * /competitions/specific/{id}:
 *   get:
 *     description: Getting specific competition
 *     tags: [Competitions]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Competition identificator
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Returned competition
 *       500:
 *         description: server error
 */
router.get('/specific/:type', async(req, res) => {
  try{
    const competitions = await Competition.findOne({
      'type': req.params.type
    });
    res.json(competitions);
  } catch (err) {
    console.error((err.message));
    res.status(500).send('Server Error');
  }
});


/**
 * @swagger
 *
 * /competitions/{competitionType}:
 *   get:
 *     description: Getting competition of certain type
 *     tags: [Competitions]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Competition type
 *         description: Competition type
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Returned type competitions
 *       500:
 *         description: server error
 */
router.get('/:type', async(req, res) => {
  try{
    const competitions = await Competition.find(
      {
        'type': req.params.type
      }
    );
    res.json(competitions);
  } catch (err) {
    console.error((err.message));
    res.status(500).send('Server Error');
  }
});

/**
 * @swagger
 *
 * /competitions:
 *   post:
 *     description: Creating a new competition
 *     tags: [Competitions]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Competition
 *         description: Competition body
 *         required: true
 *         type: Competition
 *     responses:
 *       200:
 *         description: Returned newly created competition
 *       500:
 *         description: server error
 */
router.post('/',
  async(req, res) => {

    const { name, year, type, country, places, countryCode } = req.body;

    try{
      const competition = new Competition({
        name,
        year,
        // WCH,OG,WCH20
        type,
        status: 'NEW',
        country,
        countryCode,
        places
      });
      await competition.save();
      res.json(competition);
    } catch (err) {
      console.error((err.message));
      res.status(500).send('Server Error');
    }
  });

module.exports = router;
