const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { check, validationResult } = require('express-validator/check');
const mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;

const Match = require('../models/Match');
const Team = require('../models/Teams');
const matchFunctions = require('../functions/MatchFunctions');
const drawFunctions = require('../functions/DrawFunctions');

/**
 * @swagger
 * tags:
 *   name: Matches
 *   description: Matches management
 */

/**
 * @swagger
 *
 * /match/list/{competitionId}/{competitionState}:
 *   get:
 *     description: Getting all matches from specific state in competition id
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Competition id
 *         description: Competition id
 *         required: true
 *         type: string
 *       - name: Competition State
 *         description: Competition state
 *         required: true
 *         type: string
 *       - name: Match State
 *         description: Match state
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Returned list of teams
 *       500:
 *         description: server error
 */
router.get('/list/:competitionType/:competitionState/', async(req, res) => {

    try{
        const matches = await Match.find({
            'competitionType' : req.params.competitionType,
            'competitionState': req.params.competitionState
        }).sort({
            'round': 'asc'
        });
        res.json(matches);
    } catch (err) {
        console.error((err.message));
        res.status(500).send('Server Error');
    }
});

/**
 * @swagger
 *
 * /match/{id}:
 *   get:
 *     description: Getting specific match
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Match id
 *         description: Identificator of a match
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Returned list of teams
 *       500:
 *         description: server error
 */
router.get('/:id', async(req, res) => {

    try{
        const match = await Match.findById(req.params.id);

        // Getting details about teams
        const homeInfo = await Team.findById(match.homeId);
        const awayInfo = await Team.findById(match.awayId);

        res.json({
            'matchInfo':match,
            'homeInfo':homeInfo,
            'awayInfo':awayInfo
        });
    } catch (err) {
        console.error((err.message));
        res.status(500).send('Server Error');
    }
});

/**
 * @swagger
 *
 * /match:
 *   post:
 *     description: Adding new match
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Home team
 *         description: Home team info
 *         required: true
 *         type: string
 *       - name: Away team
 *         description: Away team info
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Returned new match
 *       400:
 *         description: Problem with validation of teams
 *       500:
 *         description: server error
 */
router.post('/',
    async(req, res) => {

        let { homeId, awayId, competitionType, competitionState } = req.body;

        try{
            const match = new Match({
                homeId,
                awayId,
                competitionType,
                competitionState
            });
            await match.save();
            res.json(match);
        } catch (err) {
            console.error((err.message));
            res.status(500).send('Server Error');
        }
    }
);

/**
 * @swagger
 *
 * /match/deleteMatches/{competitionType}:
 *   delete:
 *     description: Deleting matches in a competition
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: competitionType
 *         description: Type of a competition
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success returned
 *       404:
 *         description: Matches not found
 *       500:
 *         description: server error
 */
router.delete('/deleteMatches/:competitionType', async (req, res) => {
    try{
        const Matches = await Match.find(
          {'competitionType': req.params.competitionType }
        );

        for(let i = 0; i < Matches.length; i++){
            await Match.findByIdAndDelete(Matches[i].id)
        }

        res.json({'res': 'Successfully deleted'});
    } catch(err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});


/**
 * @swagger
 *
 * /match/sim/{id}:
 *   put:
 *     description: Simulating a match
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Match ID.
 *         required: true
 *         type: string
 *       - name: phase
 *         description: Match phase.
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Returned played match
 *       404:
 *         description: Not found match
 *       500:
 *         description: Server error
 */
router.put('/sim/:id', async (req, res) => {
    const match = await Match.findById(req.params.id);
    const homeInfo = await Team.findById(match.homeId);
    const awayInfo = await Team.findById(match.awayId);

    const { homeGoals, awayGoals } = await matchFunctions.getCalculatedMatch(match, homeInfo, awayInfo);

    const newMatch = await Match.findById(req.params.id);

    res.json(newMatch);

});

/**
 * @swagger
 *
 * /match/drawGroups/{competitionId}:
 *   put:
 *     description: Drawing groups
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: competitionType
 *         description: Competition type.
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Groups were drawn
 *       500:
 *         description: Server error
 */
router.put('/drawGroups/:competitionType', async (req, res) => {

    try{
        const originalTeams = await Team
            .find({
                'competitionType': req.params.competitionType
            })
            .sort({'rating':'desc', 'power': 'desc'});
        await drawFunctions.divideMatches(originalTeams);
        res.json({'result':'Groups drawn and matches created'});
    } catch (err) {
        console.error((err.message));
        res.status(500).send('Server Error');
    }

});

/**
 * @swagger
 *
 * /match/drawMatches/{competitionType}:
 *   put:
 *     description: Drawing groups
 *     tags: [Matches]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: competitionType
 *         description: Competition type.
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Matches were drawn
 *       500:
 *         description: Server error
 */
router.put('/drawMatches/:competitionType', async (req, res) => {

    try{
        // Temporal for WCH groups
        const groupTeams = await drawFunctions.drawMatches(req.params.competitionType);
        res.json(groupTeams);
    } catch (err) {
        console.error((err.message));
        res.status(500).send('Server Error');
    }


});


module.exports = router;
