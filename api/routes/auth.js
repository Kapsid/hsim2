const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../middleware/auth');
const { check, validationResult } = require('express-validator/check');

const User = require('../models/User');


/**
 * @swagger
 * tags:
 *   name: Auth
 *   description: Auth management
 */

 /**
 * @swagger
 * /auth:
 *   get:
 *     description: Getting authorizing token
 *     tags: [Auth]
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: user.id
 *         description: User identificator.
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 *       500:
 *         description: server error
 */
router.get('/', auth, async(req, res) => {
  try{
    const user = await User.findById(req.user.id).select('-password');
    await res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

/**
 * @swagger
 *
 * /auth:
 *   post:
 *     description: Login to the application
 *     tags: [Auth]
 *     produces:
 *       - application/json
 *     parameters:
 *       - email: email
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: got token
 *       400:
 *         description: invalid credentials
 */
router.post('/', [
    check('email','Please include a valid email').isEmail(),
    check('password','Password is required').exists()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      return res.status(400).json({errors: errors.array()});
    }

    const { email, password } = req.body;

    try{
      let user = await User.findOne({ email });

      if(!user){
        return res.status(400).json({ msg: 'Invalid credentials' });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      if(!isMatch){
        return res.status(400).json({msg: 'Invalid Credentials'});
      }

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(payload, config.get('jwtSecret'), {
        expiresIn: 360000
      }, (err, token) => {
        if(err) throw err;
        res.json({token});
      });

    }

    catch(err){
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  });

module.exports = router;