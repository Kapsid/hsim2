const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { check, validationResult } = require('express-validator/check');

const Team = require('../models/Teams');

/**
 * @swagger
 * tags:
 *   name: Teams
 *   description: Teams management
 */

/**
 * @swagger
 *
 * /teams/{competitionType}:
 *   get:
 *     description: Getting all teams
 *     tags: [Teams]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Competition type
 *         description: Competition type
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Returned list of teams
 *       500:
 *         description: server error
 */
router.get('/:competitionType', auth, async(req, res) => {
  try{
    const teams = await Team
        .find({
      'competitionType': req.params.competitionType
    })
        .sort({'rating':'desc', 'power': 'desc'});
    res.json(teams);
  } catch (err) {
    console.error((err.message));
    res.status(500).send('Server Error');
  }
});

/**
 * @swagger
 *
 * /teams/{competitionType}/group/{group}:
 *   get:
 *     description: Getting teams by group and criteria for table
 *     tags: [Teams]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Competition type
 *         description: Competition type
 *         required: true
 *         type: string
 *       - name: Group name
 *         description: Group name
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Returned list of all teams with group for table purpose
 *       500:
 *         description: server error
 */
router.get('/:competitionType/group/', async(req, res) => {
  try{
    let teams = [];
    const teamsA = await Team
      .find({
        'competitionType': req.params.competitionType,
        'group': 'A'
      })
      // todo change it to compare score
      .sort({'points':'desc', 'goalsFor': 'desc', 'goalsAgainst' : 'asc', 'wins': 'desc'});

    const teamsB = await Team
        .find({
          'competitionType': req.params.competitionType,
          'group': 'B'
        })
        .sort({'points':'desc', 'goalsFor - goalsAgainst': 'desc', 'wins': 'desc'});


    teams.push(teamsA);
    teams.push(teamsB);

    res.json(teams);
  } catch (err) {
    console.error((err.message));
    res.status(500).send('Server Error');
  }
});



/**
 * @swagger
 *
 * /teams:
 *   post:
 *     description: Adding new team
 *     tags: [Teams]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: Team name.
 *         required: true
 *         type: string
 *       - name: Country code
 *         description: Shortcut of nation
 *         required: true
 *         type: string
 *       - name: Rating
 *         description: national rating
 *         required: true
 *         type: number
 *       - name: Power
 *         description: power
 *         required: true
 *         type: number
 *       - name: Competition type
 *         description: Competition type
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Returned new team
 *       400:
 *         description: Problem with validation of team
 *       500:
 *         description: server error
 */
router.post('/', [
    auth,
      check('name','Name is required').not().isEmpty(),
      check('countryCode','Country code is required').not().isEmpty(),
      check('rating','Rating is required').not().isEmpty(),
      check('competitionType','CompetitionType is required').not().isEmpty(),
      check('power','Power is required').not().isEmpty(),
  ],
  async(req, res) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()){
     return res.status(400).json({
       errors: errors.array()
     })
    }

    const { name, countryCode, rating, power, competitionType } = req.body;

    try{
      const teams = new Team({
        name,
        countryCode,
        rating,
        power,
        competitionType,
        goalsFor: 0,
        goalsAgainst: 0,
        points: 0,
        wins: 0,
        draws: 0,
        lossesInOT: 0,
        losses: 0
      });
      await teams.save();
      res.json(teams);
    } catch (err) {
      console.error((err.message));
      res.status(500).send('Server Error');
    }
});


/**
 * @swagger
 *
 * /teams/{id}:
 *   put:
 *     description: Editting a team
 *     tags: [Teams]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: Team name.
 *         required: true
 *         type: string
 *       - Country code: Country code
 *         description: Shortcut of nation
 *         required: true
 *         type: string
 *       - name: Rating
 *         description: national rating
 *         required: true
 *         type: number
 *       - name: Power
 *         description: power
 *         required: true
 *         type: number
 *       - name: Competition type
 *         description: Competition type
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Returned editted team
 *       404:
 *         description: Not found team
 *       500:
 *         description: Server error
 */
router.put('/:id', auth, async (req, res) => {
  const { name, countryCode, rating, power, competitionType } = req.body;

  // Build a contact object
  const teamFields = {};

  if(name) teamFields.name = name;
  if(countryCode) teamFields.countryCode = countryCode;
  if(rating) teamFields.rating = rating;
  if(power) teamFields.power = power;
  if(competitionType) teamFields.competitionType = competitionType;

  try{
    let team = await Team.findById(req.params.id);
    if(!team){
      return res.status(404).json({msg: 'Team not found'});
    }

    team = await Team.findByIdAndUpdate(req.params.id, {$set: teamFields}, {new: true});
    res.json(team);
  } catch(err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

/**
 * @swagger
 *
 * /teams/{id}:
 *   delete:
 *     description: Deleting a team
 *     tags: [Teams]
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: team.id
 *         description: Team identificator.
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success returned
 *       404:
 *         description: Team was not found
 *       500:
 *         description: server error
 */
router.delete('/:id', auth, async (req, res) => {
  try{
    let team = await Team.findById(req.params.id);
    if(!team){
      return res.status(404).json({msg: 'Team not found'});
    }

    team = await Team.findByIdAndDelete(req.params.id);
    res.json(team);
  } catch(err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
