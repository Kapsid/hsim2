import { combineReducers } from "redux";
import teamReducer from "./teamReducer";
import authReducer from "./authReducer";
import competitionsReducer from "./competitionsReducer";


export default combineReducers({
  team: teamReducer,
  auth: authReducer,
  competitions: competitionsReducer
});
