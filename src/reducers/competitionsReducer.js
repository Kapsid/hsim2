import {
    GET_COMPETITIONS,
    GET_COMPETITION,
    GET_GROUP_TEAMS,
    GET_ROUND_MATCHES,
    SIMULATE_MATCH
} from '../actions/types';

const initialState = {
    loading: false,
    error: null,
    competitions: null,
    competition: null,
    roundMatches: null,
    groupTeams: null
};

export default(state = initialState, action) => {
    switch(action.type) {
        case GET_COMPETITIONS:
            return {
                ...state,
                competitions: action.payload,
                loading: false
            };
        case GET_COMPETITION:
            return {
                ...state,
                competition: action.payload,
                loading: false
            };
        case GET_GROUP_TEAMS:
            return {
                ...state,
                groupTeams: action.payload,
                loading: false
            };
        case GET_ROUND_MATCHES:
            return {
                ...state,
                roundMatches: action.payload,
                loading: false
            };
        case SIMULATE_MATCH:
            return {
                ...state,
                roundMatches: state.roundMatches.map(match => match._id === action.payload.match._id ? action.payload.match : match),
                groupTeams: action.payload.teams,
                loading: false
            };
        default:
            return state;
    }
}
