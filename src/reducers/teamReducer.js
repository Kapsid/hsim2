import {GET_TEAMS, SET_LOADING, TEAMS_ERROR, ADD_TEAM, DELETE_TEAM, SET_CURRENT, CLEAR_CURRENT, UPDATE_TEAM} from '../actions/types';

const initialState = {
  teams: null,
  current: null,
  loading: false,
  error: null
};

export default(state = initialState, action) => {
  switch(action.type) {
    case GET_TEAMS:
      return {
        ...state,
        teams: action.payload,
        loading: false
      };
    case ADD_TEAM:
      return {
        ...state,
        teams: [...state.teams, action.payload],
        loading: false
      };
    case SET_CURRENT:
        return {
          ...state,
          current: action.payload
        };
    case CLEAR_CURRENT:
      return {
        ...state,
        current: null
      };
    case DELETE_TEAM:
      return {
        ...state,
        teams: state.teams.filter(team => team._id !== action.payload._id),
        loading: false
      };
    case UPDATE_TEAM:
      return {
        ...state,
        teams: state.teams.map(team => team._id === action.payload._id ? action.payload : team),
        loading: false
      };
    case SET_LOADING:
      return{
        ...state,
        loading: true
      };
    case TEAMS_ERROR:
      console.error(action.payload);
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
}