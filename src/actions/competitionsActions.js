import {
    COMPETITIONS_ERROR,
    GET_COMPETITIONS,
    COMPETITION_ERROR,
    GET_COMPETITION,
    GET_GROUP_TEAMS, GET_ROUND_MATCHES, SIMULATE_MATCH, MATCH_ERROR
} from './types';

import getHeaders from "../utils/communication/getHeaders";

// Get competitions from server
export const getCompetitions = (token) => async dispatch => {

    try{
        const res = await fetch('http://localhost:5005/api/competition/WCH',{
            headers: getHeaders(token)
        });

        const data = await res.json();

        dispatch({
            type: GET_COMPETITIONS,
            payload: data
        });

    } catch (err) {
        dispatch({
            type: COMPETITIONS_ERROR,
            payload: err.response.statusText
        });
    }
};

// Getting specific competitions
export const getCompetition = (token, competitionType) => async dispatch => {

    try{
        const res = await fetch(`http://localhost:5005/api/competition/specific/${competitionType}`,{
            headers: getHeaders(token)
        });

        const data = await res.json();

        dispatch({
            type: GET_COMPETITION,
            payload: data
        });

    } catch (err) {
        dispatch({
            type: COMPETITION_ERROR,
            payload: err.response.statusText
        });
    }
};

// Getting teams by group
export const getTeamsByGroup = () => async dispatch => {

    try{
        const res = await fetch(`http://localhost:5005/api/teams/WCH/group/`,{
        });

        const data = await res.json();
        dispatch({
            type: GET_GROUP_TEAMS,
            payload: data
        });

    } catch (err) {
        dispatch({
            type: COMPETITION_ERROR,
            payload: err.response.statusText
        });
    }
};

// Getting matches (played and unplayed)
export const getMatches = (competitionState) => async dispatch => {

    try{
        const res = await fetch(`http://localhost:5005/api/match/list/WCH/${competitionState}`,{
        });

        const data = await res.json();

        dispatch({
            type: GET_ROUND_MATCHES,
            payload: data
        });

    } catch (err) {
        dispatch({
            type: COMPETITION_ERROR,
            payload: err.response.statusText
        });
    }
};

// Simulating exact match id
export const simulateMatch = (matchId) => async dispatch => {

    try{
        const res = await fetch(`http://localhost:5005/api/match/sim/${matchId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            }
        });

        let data = {};

        data.match = await res.json();

        const resTeams = await fetch(`http://localhost:5005/api/teams/WCH/group/`,{ });

        data.teams = await resTeams.json();

        dispatch({
            type: SIMULATE_MATCH,
            payload: data
        });

    } catch (err) {
        dispatch({
            type: MATCH_ERROR,
            payload: err
        });
    }
};
