import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    USER_LOADED,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    CLEAR_ERRORS
} from '../actions/types';

// Load user
export const loadUser = (user) => async dispatch => {

    try {
        const res = await fetch('http://localhost:5005/api/users');
        const data = await res.json();
        dispatch({
            type: USER_LOADED,
            payload: data
        });
    } catch (err) {
        dispatch({ type: AUTH_ERROR});
    }
};

// Register user
export const register = (formData) => async dispatch => {
    const config = {
        headers:{
            'Content-Type': 'application/json'
        }
    };

        const res = await fetch('http://localhost:5005/api/users',{
            method: 'POST',
            body: JSON.stringify(formData),
            headers: config.headers
        });

        const data = await res.json();

        if(data.errors || data.msg) {
            dispatch({
                type: REGISTER_FAIL,
                payload: data.errors
            });

            // @todo Should not be here
            console.error('Registration failed!');
        }

        else {
            dispatch({
                type: REGISTER_SUCCESS,
                payload: data
            });

            loadUser();

        }
};

// Login user
export const login = formData => async dispatch => {
    const config = {
        headers:{
            'Content-Type': 'application/json'
        }
    };

    const res = await fetch('http://localhost:5005/api/auth',{
        method: 'POST',
        body: JSON.stringify(formData),
        headers: config.headers
    });

    const data = await res.json();

    if(data.errors || data.msg) {
        dispatch({
            type: LOGIN_FAIL,
            payload: data.errors
        });

        // @todo Should not be here
        console.error('Login failed!');
    }

    else {
        dispatch({
            type: LOGIN_SUCCESS,
            payload: data
        });

        loadUser();

    }

};

// Logout user
export const logout = () => {
    return {
        type: LOGOUT
    }
};

// Clear errors
export const clearErrors = () => {
    return {
        type: CLEAR_ERRORS
    }
};