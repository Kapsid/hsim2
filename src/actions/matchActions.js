import {
  SIMULATE_MATCH,
  MATCH_ERROR
} from './types';

import getHeaders from "../utils/communication/getHeaders";

// Simulate match
export const simulateMatch = (matchId) => async dispatch => {

  try{
    const res = await fetch('http://localhost:5005/api/competition/WCH',{
      headers: getHeaders(token)
    });

    const data = await res.json();

    dispatch({
      type: SIMULATE_MATCH,
      payload: data
    });

  } catch (err) {
    dispatch({
      type: MATCH_ERROR,
      payload: err.response.statusText
    });
  }
};


