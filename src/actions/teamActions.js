import {GET_TEAMS, SET_LOADING, TEAMS_ERROR, ADD_TEAM, DELETE_TEAM, UPDATE_TEAM, SET_CURRENT, CLEAR_CURRENT} from './types';
import getHeaders from "../utils/communication/getHeaders";

// Get teams from server
export const getTeams = (token) => async dispatch => {

  try{
    setLoading();

    const res = await fetch('http://localhost:5005/api/teams/WCH',{
      headers: getHeaders(token)
    });
    const data = await res.json();

    dispatch({
      type: GET_TEAMS,
      payload: data
    });

  } catch (err) {
    dispatch({
      type: TEAMS_ERROR,
      payload: err.response.statusText
    });
  }
};

// Add new team
export const addTeam = (team, token) => async dispatch => {

  try{
    setLoading();

    const res = await fetch('http://localhost:5005/api/teams',{
      method: 'POST',
      body: JSON.stringify(team),
      headers: getHeaders(token)
    });

    const data = await res.json();

    dispatch({
      type: ADD_TEAM,
      payload: data
    });

  } catch (err) {
    dispatch({
      type: TEAMS_ERROR,
      payload: err.response.statusText
    });
  }
};

// Delete team from server
export const deleteTeam = (id, token) => async dispatch => {

  try{
    setLoading();

    const res = await fetch(`http://localhost:5005/api/teams/${id}`, {
        method: 'DELETE',
        headers: getHeaders(token)
      });
    const data = await res.json();

    dispatch({
      type: DELETE_TEAM,
      payload: data
    });

  } catch (err) {
    dispatch({
      type: TEAMS_ERROR,
      payload: err.response.statusText
    });
  }
};

// Update team on server
export const updateTeam = (team, token) => async dispatch => {

  try{
    setLoading();

    const res = await fetch(`http://localhost:5005/api/teams/${team.id}`, {
      method: 'PUT',
      body: JSON.stringify(team),
      headers: getHeaders(token)
    });

    const data = await res.json();

    dispatch({
      type: UPDATE_TEAM,
      payload: data
    });

  } catch (err) {
    dispatch({
      type: TEAMS_ERROR,
      payload: err.response.statusText
    });
  }
};

// Set current team
export const setCurrent = team => {
  return {
    type: SET_CURRENT,
    payload: team,
  }
};

export const clearCurrent = team => {
  return {
    type: CLEAR_CURRENT
  }
};

// Set loading to true
export const setLoading = () => {
  return {
    type: SET_LOADING
  }
};
