// Teams types
export const GET_TEAMS = 'GET_TEAMS';
export const ADD_TEAM = 'ADD_TEAM';
export const DELETE_TEAM = 'DELETE_TEAM';
export const SET_CURRENT = 'SET_CURRENT';
export const CLEAR_CURRENT = 'CLEAR_CURRENT';
export const UPDATE_TEAM = 'UPDATE_TEAM';
export const CLEAR_TEAM = 'CLEAR_TEAMS';
export const SET_LOADING = 'SET_LOADING';
export const TEAMS_ERROR = 'TEAMS_ERROR';
export const SEARCH_TEAMS = 'SEARCH_TEAMS';

// Auth types
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const USER_LOADED = 'USER_LOADED';
export const AUTH_ERROR = 'AUTH_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT = 'LOGOUT';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';

// Competition types
export const GET_COMPETITIONS = 'GET_COMPETITIONS';
export const COMPETITIONS_ERROR = 'COMPETITIONS_ERROR';
export const GET_COMPETITION = 'GET_COMPETITION';
export const COMPETITION_ERROR = 'COMPETITION_ERROR';
export const GET_GROUP_TEAMS = 'GET_GROUP_TEAMS';
export const GENERATE_MATCHES = 'GENERATE_MATCHES';
export const GET_ROUND_MATCHES = 'GET_GROUP_MATCHES';

// Match types
export const SIMULATE_MATCH = 'SIMULATE_MATCH';
export const MATCH_ERROR = 'MATCH_ERROR';
