import React, { Fragment,useEffect } from 'react';
import Teams from "./components/teams/Teams";
import AddTeamModal from "./components/teams/AddTeamModal";
import EditTeamModal from "./components/teams/EditTeamModal";
import { Provider } from "react-redux";
import store from './store';
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import './resources/styles/styles.scss';
import Navbar from "./components/layout/Navbar";
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import About from "./components/pages/about";
import Competitions from "./components/competitions/Competitions";
import CompetitionDetail from "./components/competitions/detail/CompetitionDetail";


const App = () => {
  useEffect(() => {
    // Init Materialize JS
    M.AutoInit();
    let el = document.querySelector('.tabs');
    let instance = M.Tabs.init(el, {});
  });
  return (
    <Provider store={store}>
      <Router>
      <Navbar/>
        <Fragment>
          <div className='container'>
            <AddTeamModal/>
            <EditTeamModal/>
            <Switch>
              <Route exact path='/' component={About} />
              <Route exact path='/teams' component={Teams} />
              <Route exact path='/register' component={Register} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/competitions' component={Competitions} />
              <Route exact path='/competitions/:id' component={CompetitionDetail} />
            </Switch>
          </div>
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;
