/* Getting authorized headers */
export default function getAuthorizedHeaders(token) {
    return {
                'Content-Type': 'application/json',
                'x-auth-token': token,
        }
};