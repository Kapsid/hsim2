import React from 'react';

const About = () => {
  return (
      <div className='container'>
          <h1>
              Ice hockey simulator
          </h1>
          <p>
              This is simulator for World Championships, Euro hockey tour and Olympic Games. NHL and national leagues are coming.<br /><br />
              Developed by <a href='https://www.linkedin.com/in/martin-urbanczyk-0a9355111/'>Martin Urbanczyk</a>. <br /> <br />
              Any comments can be made to <a href='mailto:urbanczyk.martin@seznam.cz'>urbanczyk.martin@seznam.cz</a>.
          </p>
      </div>
  )
};

export default About;

