import React, { useState } from 'react';
import {connect} from "react-redux";
import { useHistory } from "react-router-dom";
import { loadUser, register, logout } from "../../actions/authActions";
import Swal from 'sweetalert2';
import useUpdateEffect from "../../utils/customHooks/useUpdateEffect";

const Register = ({ auth, register }) => {

    let history = useHistory();

    useUpdateEffect(() => {
        if(auth.isAuthenticated){
            history.push('/teams');
        }

        else{
            if(auth.error){
                auth.error.map(error => {
                    return Swal.fire({
                        icon:'error',
                        title:error.msg
                    });
                });
            }
            else{
                Swal.fire({
                    icon:'error',
                    title:'Register failed'
                });
            }
        }

    }, [auth]);

    const [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        password2: ''
    });

    const { name, email, password, password2} = user;

    const onChange = e => {
        setUser({ ...user, [e.target.name]: e.target.value});
    };

    const onSubmit = e => {
        e.preventDefault();
        if(name === '' || email === '' || password === ''){
            Swal.fire('Please enter all fields');
        } else if(password !== password2){
            Swal.fire('Passwords do not match');
        } else {
            register({
                name,
                email,
                password
            });
        }
    };

    return (
        <div className='form-container'>
            <h1>Register</h1>
            <form>
                <div className='form-group'>
                    <label htmlFor='name'>Name</label>
                    <input type='text' name='name' value={name} onChange={onChange} required />
                </div>
                <div className='form-group'>
                    <label htmlFor='email'>Email Address</label>
                    <input type='email' name='email' value={email} onChange={onChange} required />
                </div>
                <div className='form-group'>
                    <label htmlFor='password'>Password</label>
                    <input type='password' name='password' value={password} onChange={onChange} required minLength='6'/>
                </div>
                <div className='form-group'>
                    <label htmlFor='password2'>Confirm Password</label>
                    <input type='password' name='password2' value={password2} onChange={onChange} required minLength='6'/>
                </div>
                <input type='submit' value='Register' className='btn btn-primary btn-block' onClick={onSubmit} />
            </form>
        </div>
    );
};

const mapStateToProps = state => ({
    user: state.user,
    loading: state.team.loading,
    auth: state.auth,
    error: state.error
});

export default connect(mapStateToProps,
    {
        loadUser,
        register,
        logout
    }
)(Register);