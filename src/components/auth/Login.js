import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import { loadUser, login } from "../../actions/authActions";
import useUpdateEffect from "../../utils/customHooks/useUpdateEffect";
import Swal from 'sweetalert2';

const Login = ({ auth, login }) => {

    const [user, setUser] = useState({
        email: '',
        password: ''
    });

    let history = useHistory();

    const { email, password } = user;

    useUpdateEffect(() => {

        if (auth.isAuthenticated) {
            history.push('/teams');
        }

        else{
            if(auth.error){
                auth.error.map(error => {
                    return Swal.fire({
                        icon:'error',
                        title:error.msg
                    });
                });
            }
            else{
                Swal.fire({
                    icon:'error',
                    title:'Login failed'
                });
            }
        }
    }, [auth]);


    const onChange = e => {
        setUser({ ...user, [e.target.name]: e.target.value});
    };

    const onSubmit = async e => {
        e.preventDefault();
        if(email === '' || password === '') {
            await Swal.fire('Please fill in all fields');
        } else {
            await login({
                email,
                password
            });
        }
    };

    return (
        <div className='form-container'>
            <h1>Login</h1>
            <form>
                <div className='form-group'>
                    <label htmlFor='email'>Email Address</label>
                    <input type='email' name='email' value={email} onChange={onChange} required />
                </div>
                <div className='form-group'>
                    <label htmlFor='password'>Password</label>
                    <input type='password' name='password' value={password} onChange={onChange} required />
                </div>
                <input type='submit' value='Login' className='btn btn-primary btn-block' onClick={onSubmit} />
            </form>
        </div>
    );
};

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
    loading: state.team.loading,
    error: state.error
});

export default connect(mapStateToProps,
    {
        loadUser,
        login
    }
)(Login);