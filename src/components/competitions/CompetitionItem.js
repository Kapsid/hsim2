import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const CompetitionItem = ({ competition }) => {

    const countryImage = require(`../../resources/img/flags/${competition.countryCode}.svg`);

    return (
            <li className=''>
                <div>
                    <h5>{competition.name}</h5>
                    <div className='row'>
                        <div className='col m4'>
                            <p>
                                {competition.places.map(
                                    place => <strong> {place}<br /> </strong>
                                )}
                            </p>
                        </div>
                        <div className='col m8'>
                            <img className='teams-competition-flag' src={countryImage} alt='logo' />
                        </div>
                    </div>

                </div>
            </li>
    );
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, {})(CompetitionItem);
