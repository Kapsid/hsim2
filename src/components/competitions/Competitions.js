import React, {Fragment, useEffect} from 'react';
import {Link, useHistory} from "react-router-dom";
import { connect } from 'react-redux';
import Preloader from "../layout/Preloader";
import {getCompetitions} from "../../actions/competitionsActions";
import AddBtn from "../layout/AddBtn";
import CompetitionItem from "./CompetitionItem";

const Competitions = ({ loading, competitions, getCompetitions, auth}) => {

    let history = useHistory();

    useEffect(() => {

        // Check if is authorized //@todo change it
        if (auth.isAuthenticated === false) {
            history.push('/login');
        } else {
            getCompetitions(auth.user.token);
        }

    },[auth]);

    if(loading || competitions === null){
        return <Preloader/>;
    }

    return (
        <Fragment>
            <AddBtn/>
            <ul className='collection with-header'>
                <li className='collection-header'>
                    <h4 className='center'>World Championships</h4>
                </li>
                {!loading && competitions && competitions.length === 0 ? (<p className='center'>No competitions to show...</p>):
                    competitions.map(competition =>
                      <Link to={`/competitions/${competition._id}`} className='competition-link collection-item-competition collection-item'>
                        <CompetitionItem competition={competition} key={competition._id} />
                      </Link>
                      )
                }
            </ul>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    auth: state.auth,
    competitions: state.competitions.competitions,
    loading: state.team.loading
});

export default connect(mapStateToProps,
    {
        getCompetitions
    }
)(Competitions);
