import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import CompetitionDetailTabs from "./CompetitionDetailTabs";

const CompetitionDetailHeader = ({ competition }) => {

  return (
    <li className=''>
      <div>
        <h4 className='text-center'>{competition.name}</h4>
        <p>
          <strong>Hosted by :</strong>{competition.country} ({competition.places.map(
            place => <span> {place}, </span>
        )})
        </p>

        {competition.status === 'NEW' &&
          <button className='btn btn-large btn-primary centered'>
            Draw matches and groups
          </button>
        }

        {competition.status === 'GROUP' &&
          <CompetitionDetailTabs />
        }
      </div>
    </li>
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {})(CompetitionDetailHeader);
