import React, {Fragment} from 'react';
import {simulateMatch} from "../../../actions/competitionsActions";
import {connect} from "react-redux";
import M from "materialize-css/dist/js/materialize.min";
const CompetitionMatchItem = ({ match, simulateMatch }) => {

  const simulate = () => {
    simulateMatch(match._id);
    M.toast({ html: 'Match simulated!'});
  };

  return (
    <tr>
      <td>{match.round + 1}</td>
      <td>{match.homeName}</td>
      <td>{match.awayName}</td>
      <td>
        {match.matchState === 'NEW' && <button className='btn btn-primary' onClick={simulate}>Simulate</button> }
        {match.matchState !== 'NEW' &&
          <span>{match.homeGoals} : {match.awayGoals}</span>
        }
      </td>
    </tr>
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  simulateMatch
})(CompetitionMatchItem);
