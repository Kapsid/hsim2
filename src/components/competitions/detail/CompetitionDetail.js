import React, {Fragment, useEffect} from 'react';
import {connect} from "react-redux";
import {getCompetition, getTeamsByGroup, getMatches} from "../../../actions/competitionsActions";
import { useHistory } from "react-router-dom";
import CompetitionDetailHeader from "./CompetitonDetailHeader";
import useUpdateEffect from "../../../utils/customHooks/useUpdateEffect";

const CompetitionDetail = ({ competition, getCompetition, getTeamsByGroup, auth}) => {

    let history = useHistory();

    useEffect(() => {

        // Check if is authorized //@todo change it
        if (auth.isAuthenticated === false) {
          history.push('/login');
        } else {
          getCompetition(auth.user.token,'WCH');
          getTeamsByGroup(auth.user.token,'A');
          getTeamsByGroup(auth.user.token,'B');
        }

    }, [getCompetition]);
    return (
        <div className='competition-detail'>
          {competition &&
            <CompetitionDetailHeader competition={competition} key={competition._id} />}
        </div>
    );
};

const mapStateToProps = state => ({
    auth: state.auth,
    competition: state.competitions.competition,
    groupTeams: state.competitions.groupTeams
});

export default connect(mapStateToProps,
  {
    getCompetition, getTeamsByGroup
  }
)(CompetitionDetail);
