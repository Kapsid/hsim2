import React, {Fragment} from 'react';

const CompetitionDetailGroup = ({teams, groupIndex}) => {

    let groupTeams = [];

    // If teams are loaded
    if(teams) groupTeams = teams[groupIndex];


    return (
        <Fragment>
            <table>
                <thead>
                    <tr>
                        <th>Place</th>
                        <th>Country</th>
                        <th>Name</th>
                        <th>Wins</th>
                        <th>Draws</th>
                        <th>Losses</th>
                        <th>Score</th>
                        <th>Points</th>
                    </tr>
                </thead>
                <tbody>
                    {groupTeams &&
                        groupTeams.map((team,index) =>
                        <tr className={`teams-row-${index}`}>
                            <td>{index + 1}.</td>
                            <td><img className='group-team-image' src={require(`../../../resources/img/flags/${team.countryCode}.svg`)} alt='logo' /></td>
                            <td>{team.name}</td>
                            <td>{team.wins}</td>
                            <td>{team.draws}</td>
                            <td>{team.losses}</td>
                            <td>{team.goalsFor}:{team.goalsAgainst}</td>
                            <td>{team.points}</td>
                        </tr>)
                    }
                </tbody>
            </table>
        </Fragment>
    );
};

export default CompetitionDetailGroup;

