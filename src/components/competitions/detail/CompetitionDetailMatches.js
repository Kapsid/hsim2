import React, {Fragment, useEffect} from 'react';
import { connect } from 'react-redux';
import { getMatches} from "../../../actions/competitionsActions";
import CompetitionMatchItem from "./CompetitionMatchItem";

const CompetitionDetailMatches = ({ getMatches, matches}) => {

    useEffect(() => {
          getMatches('GROUP');
    }, [getMatches]);

    return (
        <div>
            <h4>Matches</h4>
          <table>
            <thead>
              <tr>
                <th>Round</th>
                <th>Home</th>
                <th>Away</th>
                <th>Score</th>
              </tr>
            </thead>
            <tbody>
              {matches && matches.map(match => <CompetitionMatchItem key={match._id} match={match} />)}
            </tbody>
          </table>


        </div>
    );
};

const mapStateToProps = state => ({
    auth: state.auth,
    competition: state.competitions.competition,
    matches: state.competitions.roundMatches
});

export default connect(mapStateToProps,
  {
    getMatches
  }
)(CompetitionDetailMatches);
