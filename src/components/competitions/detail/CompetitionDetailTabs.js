import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import M from 'materialize-css/dist/js/materialize.min.js';
import CompetitionDetailGroup from "./CompetitionDetailGroup";
import CompetitionDetailMatches from "./CompetitionDetailMatches";
import {getMatches, getTeamsByGroup} from "../../../actions/competitionsActions";

const CompetitionDetailTabs = ({ groupTeams, roundMatches }) => {

    useEffect(() => {
        let el = document.querySelector('.tabs');
        M.Tabs.init(el, {});
    });

    return (<div className="row">
                <div className="col s12">
                    <ul className="tabs">
                        <li className="tab col s3"><a href="#groupA">Group A</a></li>
                        <li className="tab col s3"><a href="#groupB">Group B</a></li>
                        <li className="tab col s3"><a href="#matches">Matches</a></li>
                        <li className="tab col s3"><a href="#results">Results</a></li>
                    </ul>
                </div>
                <div id="groupA" className="col s12">
                    <CompetitionDetailGroup groupName='A' teams={groupTeams} groupIndex={0} />
                </div>
                <div id="groupB" className="col s12">
                    <CompetitionDetailGroup groupName='B' teams={groupTeams} groupIndex={1} />
                </div>
                <div id="matches" className='col s12'>
                  <CompetitionDetailMatches />
                </div>
                <div id="results" className='col s12'>
                </div>
            </div>
    );
};


const mapStateToProps = state => ({
    groupTeams: state.competitions.groupTeams,
    roundMatches: state.competitions.roundMatches
});


export default connect(mapStateToProps,
    {
        getTeamsByGroup,
        getMatches
    }
)(CompetitionDetailTabs);
