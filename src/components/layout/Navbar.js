import React, {Fragment} from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import IIHF from '../../resources/img/IIHF.png';
import { connect } from 'react-redux';
import { logout } from "../../actions/authActions";



const Navbar = ({ logout, auth }) => {
    let history = useHistory();

    const onLogout = () => {
        logout();
        history.push('/login');
    };


    const guestLinks = (
        <Fragment>
            <li>
                <Link to='/login'>Login</Link>
            </li>
            <li>
                <Link to='/register'>Register</Link>
            </li>
        </Fragment>
    );

    const authLinks = (
        <Fragment>
            <li>
                <Link to='/teams'>Teams</Link>
            </li>
            <li>
                <Link to='/competitions'>World Championships</Link>
            </li>
            <li>
                <a onClick={onLogout} href='!#'>
                    Logout
                </a>
            </li>
        </Fragment>
    );

    return (
      <nav>
        <div className="nav-wrapper">
          <a href="!#" className="brand-logo"><img src={IIHF} alt='logo' /></a>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
              {auth.isAuthenticated ? authLinks : guestLinks}
          </ul>
        </div>
      </nav>
    );
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps,
    {
        logout
    })(Navbar);