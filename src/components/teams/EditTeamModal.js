import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import M from 'materialize-css/dist/js/materialize.min.js';
import { updateTeam } from "../../actions/teamActions";

const EditTeamModal = ({ current, auth, updateTeam }) => {
  const [name, setName] = useState('');
  const [id, setId] = useState('');
  const [countryCode, setCountryCode] = useState('');
  const [rating, setRating] = useState('');
  const [power, setPower] = useState('');
  const [competitionType, setCompetitionType] = useState('');

  useEffect(() => {
    if(current) {
      setName(current.name);
      setId(current._id);
      setCountryCode(current.countryCode);
      setRating(current.rating);
      setPower(current.power);
      setCompetitionType(current.competitionType);
    }
  }, [current]);

  const onSubmit = () => {
    if(name === ''){
      M.toast({ html: 'Please enter a name' });
    }
    else{
      const updatedTeam = {
        id: current._id,
        name,
        countryCode,
        rating,
        power,
        competitionType
      };

      updateTeam(updatedTeam,auth.token);
      M.toast({ html: `Team updated`});

      // Clear fields
      setName('');
      setId('');
      setCountryCode('');
      setRating('');
      setPower('');
      setCompetitionType('');
    }
  };

  return (
      <div id='edit-team-modal' className='modal' style={modalStyle}>
        <div className='modal-content'>
          <h4>Editting team {name}</h4>
          <div className='row'>
            <input type='text' name='message' value={name} onChange={e => setName(e.target.value)} />
            <label htmlFor='message' className='active'>
              Team Name
            </label>
          </div>

          <div className='row'>
            <input type='text' name='countryCode' value={countryCode} onChange={e => setCountryCode(e.target.value)} />
            <label htmlFor='countryCode' className='active'>
              Country code
            </label>
          </div>

          <div className='row'>
            <input type='text' name='rating' value={rating} onChange={e => setRating(e.target.value)} />
            <label htmlFor='rating' className='active'>
              Rating
            </label>
          </div>

          <div className='row'>
            <input type='text' name='rating' value={power} onChange={e => setPower(e.target.value)} />
            <label htmlFor='rating' className='active'>
              Power
            </label>
          </div>

          <div className='row'>
            <div className='input-field'>
              <select name='competitionType' value={competitionType} className='browser-default' onChange={e => setCompetitionType(e.target.value)}>
                <option value='' disabled>
                  Choose a type
                </option>
                <option value='WCH'>
                  World Championships
                </option>
              </select>
            </div>
          </div>


        </div>
        <div className='modal-footer'>
          <a href='#!' onClick={onSubmit} className='modal-close waves-effect blue waves-light btn-flat'>
            Enter
          </a>
        </div>
      </div>
  );
};

const modalStyle = {
  width: '75%',
  height: '75%'
};

const mapStateToProps = state => ({
  auth: state.auth,
  current: state.team.current
});

  export default connect(mapStateToProps, { updateTeam }) (EditTeamModal);