import React from 'react';
import { connect } from 'react-redux';
import { deleteTeam, setCurrent } from "../../actions/teamActions";

import M from 'materialize-css/dist/js/materialize.min';

const TeamItem = ({ team, deleteTeam, setCurrent, auth }) => {
  const onDelete = () => {
    deleteTeam(team._id, auth.token);
    M.toast({ html: 'Team Deleted'});
  };

  const countryImage = require(`../../resources/img/flags/${team.countryCode}.svg`);

    return (

      <li className='collection-item'>
        <div>
          <a href='#edit-team-modal' className='modal-trigger teams-name' onClick={() => setCurrent(team)}>
            <img className='teams-flag' src={countryImage} alt='logo' /> {team.name}
          </a> - Power: {team.power}, Rating: {team.rating}
          <span className='grey-text'>
          </span>
          <a href='#!' onClick={onDelete} className='secondary-content'><i className='material-icons white-text'>delete</i></a>
        </div>
      </li>
    );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deleteTeam, setCurrent })(TeamItem);