import React, {Fragment, useEffect} from 'react';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import TeamItem from "./TeamItem";
import Preloader from "../layout/Preloader";
import {getTeams} from "../../actions/teamActions";
import AddBtn from "../layout/AddBtn";

const Teams = ({ team: { teams, loading }, getTeams, auth}) => {

  let history = useHistory();

  useEffect(() => {

    // Check if is authorized //@todo change it
    if (auth.isAuthenticated === false) {
      history.push('/login');
    } else {
      getTeams(auth.user.token);
    }

  },[auth]);

  if(loading || teams === null){
    return <Preloader/>;
  }

  return (
      <Fragment>
        <AddBtn/>
        <ul className='collection with-header'>
          <li className='collection-header'>
            <h4 className='center'>Teams by rating</h4>
          </li>
          {!loading && teams.length === 0 ? (<p className='center'>No teams to show...</p>):
              teams.map(team => <TeamItem team={team} key={team._id} />)
          }
        </ul>
      </Fragment>
  );
};

const mapStateToProps = state => ({
  auth: state.auth,
  team: state.team,
  loading: state.team.loading
});

export default connect(mapStateToProps,
    {
      getTeams
    }
)(Teams);